# AV2 - Projeto Backend Singleton
## Descrição do Projeto
<p align ="center">Padrões de projeto (Singleton)</p>

<h4 align="center"> Projeto em desenvolvimento ⚠️ </h4>

### Pré-requisitos

Primeiramente para utilizar a aplicação você precisa configurar seu ambiente \n de desenvolvimento instalando as ferramentas: 
[Node.js](https://nodejs.org/en/)
Editor de texto recomendado [VSCode](https://code.visualstudio.com/)

```bash

# Clone o repositório
$ git clone <https://gitlab.com/Vitor_Liaz88/av2_-backend.git>

# Instale as depêndencias
$ npm install

# Instale o typescript em modo de desenvolvimento
$ npm install typescript -d

# Execute a aplicação
$ yarn dev

```
### Tecnologias

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
