import { Logger } from './logger';

export class StudentRegister {
  //declara uma variavel contendo o objeto logger
  private registerLogger: Logger = Logger.getInstance();
  constructor() {
    //Ao iniciar o objeto chamamos a função de log
    this.registerLogger.log('Instancia do registrador criada.');
  }
  //processador de matricula
  public process(registration: number): void {
    this.registerLogger.log('Processando matricula: ' + registration.toString());
  }
}
