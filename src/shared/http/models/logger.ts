// Logger.ts
export class Logger {
  private logs: object[];
  // Criar um objeto do tipo da classe como static
  private static instance: Logger;
  // Uso do construtor como privado
  private constructor() {
    //Recebe os logs e acumula durante a existencia do objeto
    this.logs = [];
  }
  get count(): number {
    //retorna a quantidade de logs gerados dentro do objeto
    return this.logs.length;
  }
  // Função de instanciação do objeto
  public static getInstance(): Logger {
    if (!Logger.instance) {
      //caso não exista uma instancia desta
      Logger.instance = new Logger();
    }
    return Logger.instance;
  }

  // recebe como parametro a menssagem a ser imprimida
  // e gera uma data para registrar o momento da requisição
  log(message: string) {
    const timestamp: string = new Date().toISOString();
    this.logs.push({ message, timestamp });
    console.log(`${timestamp} - ${message}`);
  }
}
