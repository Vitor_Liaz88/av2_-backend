import { Logger } from './models/logger';
import { StudentRegister } from './models/student-register';
//O ponto de acesso instancia  esse objeto pela primeira vez
const myLogger: Logger = Logger.getInstance();
//ao instancia-lo ele passa uma mensagem para ser printada no console
myLogger.log('Iniciando processamento de pagamento.');
//conta o número de logs
console.log('Contador: ' + myLogger.count.toString());
//instancia um objeto do tipo pagamento
const myRegister: StudentRegister = new StudentRegister();
//simulacao de pagamento
myRegister.process(1815280043);
//emite a quantidade de logs criados
console.log('Contador: ' + myLogger.count.toString());
